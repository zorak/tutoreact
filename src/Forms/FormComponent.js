import React from 'react'

function FormComponent(props){
    
    return(
        <div>
            <form>
                <h2>Persona</h2>
                <br/>
                <label>Nombre:</label>
                <input type="text" name = "firstName" value = {props.data.firstName} placeholder = "FirstName" onChange={props.handleChange}/>
                <br/>
                <label>Apellido:</label>
                <input type="text" name ="lastName" value = {props.data.lastName} placeholder = "LastName" onChange = {props.handleChange}/>
                <br/>
                <label>Edad:</label>
                <input type="number" name= "age" value = {props.data.age} placeholder = "Edad" onChange = {props.handleChange}/>
                <br/>
                <label>Descripción:</label>
                <br/>
                <textarea />
                <br/>
                <input type ="checkbox" name = "isFriend" checked={props.data.isFriend} onChange={props.handleChange}/>Amigo?
                <br/>
                <label>Género:</label>
                <br/>
                <input type="radio" name ="gender" value ="Masculino" checked ={props.data.gender ==="Masculino"} onChange={props.handleChange}/>Masculino
                <br/>
                <input type="radio" name ="gender" value ="Femenino" checked ={props.data.gender==="Femenino"} onChange={props.handleChange}/>Femenino
                <br/>
                <label>
                    Color Favorito:
                </label>
                <select value={props.data.favColor} name="favColor" onChange={props.handleChange}>
                    <option value = "">--Selecciona un color--</option>
                    <option value = "red" >red</option>
                    <option value = "blue" >blue</option>
                    <option value = "yellow" >yellow</option>
                    <option value = "black" >black</option>
                    <option value = "pink" >pink</option>
                </select>
                <br></br>
                <h2>Destino viaje</h2>
                <select value = {props.data.destination} name="destination" onChange={props.handleChange}>
                    <option value="">--Selecciona un destino--</option>
                    <option value="Germany">Germany</option>
                    <option value= "Norway">Norway</option>
                    <option value="North Pole">North Pole</option>
                    <option value= "South Pole">South Pole</option>
                </select>
                <br/>
                <label>Tipo de dieta</label>
                <br/>
                <label>
                <input type="checkbox" name="isVegan" checked={props.data.isVegan} onChange={props.handleChange}/>Vegana
                </label>
                <br/>
                <label>
                <input type="checkbox" name="isKosher" checked={props.data.isKosher} onChange={props.handleChange}/>Kosher
                </label>
                <br/>
                <label>
                <input type="checkbox" name="isLactoseFree" checked={props.data.isLactoseFree} onChange={props.handleChange}/>Lactose free
                </label>
                <br/>
                <label>
                <input type="checkbox" name="isVegetarian" checked={props.data.isVegetarian} onChange={props.handleChange}/>Vegetariana
                </label>
                <br/>
                <label>
                <input type="checkbox" name="isAlergic" checked={props.data.isAlergic} onChange={props.handleChange}/>Alergico a alimentos
                </label>
                <br/>



                    
                    


                <br/>
                <button>Submit</button>
            </form>

            <h1>Detalles</h1>
            <p>Nombre: {props.data.firstName} {props.data.lastName}</p>
            <p>Edad: {props.data.age}</p>
            <p>Amigo? {props.data.isFriend ? "Si" : "No" }</p> 
            <p>Color favorito: {props.data.favColor}</p>
            <p>Género: {props.data.gender}</p>
            <p>Destino: {props.data.destination}</p>
            <p>Restricción de dieta:</p>
            <p>Vegan:{props.data.isVegan ? "Yes":"No"}</p>
            <p>Kosher:{props.data.isKosher ? "Yes":"No"}</p>
            <p>Lactose Free:{props.data.isLactoseFree ? "Yes":"No"}</p>
            <p>Vegetarian:{props.data.isVegetarian ? "Yes":"No"}</p>
            <p>Alergic:{props.data.isAlergic ? "Yes":"No"}</p>
        </div>
    )
}
export default FormComponent