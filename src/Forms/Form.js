import React, {Component} from 'react'
import FormComponent from './FormComponent'

class Form extends Component{
    constructor(){
        super()
        this.state={
            firstName:"", 
            lastName:"", 
            age : "",
            isFriend:true, 
            gender:"", 
            favColor:"",
            destination:"",
            isVegan:false,
            isKosher:false,
            isLactoseFree:false,
            isVegetarian:false,
            isAlergic:false
        }
        this.handleChange = this.handleChange.bind(this)
    }
    //En react, se mantiene toda la informacion de manera dinamica en state, en vez de solo tomar la ultima data

    handleChange(event){
        const {name,value, type, checked} = event.target
        if (type == "checkbox")
            this.setState({[name]:checked})
        else
            this.setState({
                //setear cada atributo de state puede ser engorroso
                //se recomuenda llamar al name del objeto
                //firstName : event.target.value
                [name]:value
            })
    }
    render(){
        return(
            <FormComponent handleChange={this.handleChange} data={this.state}/>
        )
    }

}

export default Form