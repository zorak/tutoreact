import React from "react"


class StateApp extends React.Component{
    //Para generar estados (states) es necesario tener un constructor
    //para inicializar partes de la clase
    constructor(){
        super();
        this.state = {
            answer : "yes",
            passData:"Hola"
        }
        //La informacion de state se puede pasar a las clases hijas 
        //mediante props
    }
    render(){
        return(
            <div>
                <h1>State App</h1>
                <h2>Is state important to know??</h2>
                <h2>{this.state.answer}</h2>
                <StateChild answer={this.state}/>
            </div>
        )
    }
}

class StateChild extends React.Component{
    constructor(){
        super()
    }
    render(){
        return(
            <div>
                <h2>Componente con variables heredadas</h2>
                <h2>{this.props.answer.passData}</h2>
            </div>
        )
    }

}

export default StateApp