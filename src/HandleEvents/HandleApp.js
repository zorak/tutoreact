import React from "react"

//los metodos no se llaman con "" como html, sino que
// en {} y se usa onClick={}
class HandleApp extends React.Component{
    render(){
        return(
            <div>
                <img onMouseOver={mouseOver} onMouseOut={mouseOut} src="https://vignette.wikia.nocookie.net/asdfmovie/images/c/cd/Stegosaurus.png/revision/latest?cb=20120801045539">
                </img>
                <br />
                <br />
                <button onClick={clic}>Click Me</button>
            </div>
        )
    }
}

function clic(){
    console.log("I was clicked")
}
function mouseOver(){
    console.log("Mouse over photo")
}
function mouseOut(){
    console.log("Mouse out of photo")
}
export default HandleApp