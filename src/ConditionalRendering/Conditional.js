import React from 'react'

class Conditional extends React.Component{
    render(){
        return(
            <h1>Valor isLoading = false</h1>
        )

        //Return trabajan la logica aca. No recomendado

        // condition ? statement if true: statement if false
        //Para utilizar este formato, de debe mantener dentro del return
        //y dentro de un div, considerar mantener entre llaves por ser 
        //elementos JS
        // return(
        //     <div>
        //         {this.props.isLoading ? <h1>Loading...</h1> : <h1>codigo desde llave</h1>}
        //     </div>
        // )

        // if (this.props.isLoading){
        //     return(
        //         <div>
        //             Loading...
        //         </div>
        //     )
        // }
        // return(
        //     <div>
        //         codigo dkasdakads
        //     </div>
        // )
    }
}


// function Conditional(props){
//     if(props.isLoading === true){
//         return (
//             <h1>Loading...</h1>
//         )
//     }else{
//         return(
//             <div>
//                 codigo
//             </div>
//         )
//     }
// }


export default Conditional