import React from 'react'
import Conditional from './Conditional'

class App extends React.Component{
    constructor(){
        super()
        this.state={
            isLoading :true
        }
    }

    //cada ves que el componente se carga, espera un 1.5 segundos para cambiar el
    //valor de isLoading. El cambio genera que se actualice el componente Conditional
    componentDidMount(){
        setTimeout(()=>{
            this.setState({isLoading:false})
        },1500)
    }

    render(){
        //Buena practica de código: generar la logica en app, y de aca se determina si el componente hijo
        //es renderizado o no
        return(
            <div>
                {this.state.isLoading ? <h1>Loading...</h1>: <Conditional />}
            </div>
        )


        //este return manda el value de state para que se genere la logica
        //en el componenete hijo
        return(
            <div>
                <h1>Header</h1>
                <Conditional isLoading={this.state.isLoading}/>
                <h1>Footer</h1>
            </div>
        )
    }
}

export default App