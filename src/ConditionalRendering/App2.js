import React from 'react'

class App2 extends React.Component{
    constructor(){
        super()
        this.state={
            unreadMessages:[
                "Call your mom!",
                "New spam mail available"
            ]
        }
    }

    render(){
        //seccion entre llaves permite generar una condicionalidad directa sin encerrar todo en un if
        //si la condicion de la izq (n>0) no se cumple, la seccion de la derecha no se renderiza
        return(
            <div>
                <h2>You have {this.state.unreadMessages.length} messages!</h2>
                <h1>-------------------------</h1>
                {this.state.unreadMessages.length > 0 &&
                <h2>You have {this.state.unreadMessages.length} messages!</h2>}
            </div>
        )
    }
}



export default App2