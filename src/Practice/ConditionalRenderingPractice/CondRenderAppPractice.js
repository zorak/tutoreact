import React from 'react'

class CondRenderApp extends React.Component{
    constructor(){
        super()
        this.state={logIn:false}
        this.handleClick=this.handleClick.bind(this)
    }
    //adasd

    handleClick(){
        this.setState(prevState =>{return{logIn:!prevState.logIn}})
    }

    render(){
        let buttonText =this.state.logIn ? <h6>Log out</h6>:<h6>Log in</h6> 
        let loggedText = this.state.logIn ? <h3>You are logged in</h3>:<h3>You are logged out</h3>
        return(
            <div>
                <button onClick={this.handleClick}>
                    {buttonText}
                </button>

                { loggedText }
                
            </div>
        )
    }
    
}

export default CondRenderApp
