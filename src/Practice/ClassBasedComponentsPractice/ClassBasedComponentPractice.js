import React from 'react'

/*
function ClassBasedComponentPractice(){
    return(
        <div>
            <Header />
            <Greeting />
        </div>
    )
}
*/
class ClassBasedComponentPractice extends React.Component{
    render(){
        return(
            <div>
                <Header username="jose"/>
                <Greeting />
            </div>
        )
    }
}

/*
function Header(props){
    return (
        <header>
            <p>Welcome, {props.username}</p>
        </header>
    )
}
*/
//al llamar a props DEBE ir con this.
class  Header extends React.Component{
    render(){
        return (
            <header>
                <p>Welcome, {this.props.username}</p>
            </header>
        )
    }
    
}
/*
function Greeting(){
    const date = new Date()
    const hours = date.getHours()
    let timeOfDay
    if (hours<12){
        timeOfDay ='morning'
    }else if (hours>=12 && hours<17){
        timeOfDay = 'afternoon'
    }else{
        timeOfDay='night'
    }

    return (
        <h1>Good {timeOfDay} to you, Sir or madam!</h1>
    )
}
*/

//Clases dependientes extienden desde React.Component
class Greeting extends React.Component{
    render(){
        const date = new Date()
        const hours = date.getHours()
        let timeOfDay
        if (hours<12){
            timeOfDay ='morning'
        }else if (hours>=12 && hours<17){
            timeOfDay = 'afternoon'
        }else{
            timeOfDay='night'
        }
    
        return (
            <h1>Good {timeOfDay} to you, Sir or madam!</h1>
        )
    }
    
}

export default ClassBasedComponentPractice