import React from 'react'
import Joke from './Joke'

function JokeApp(){
    let jokes = [
        ["por qué la gallina cruzó la calle?","para llegar al otro lado"],
        ["un pollito levanto una pata, levanto la otra y se cayó"]
        ]
    return(
        <div>
            <p>chiste 1</p>
            <Joke chistes={{joke:jokes[0][0],question:"por qué?",punchline:jokes[0][1]}} />
            <p>chiste 2</p>
            <Joke chistes={{joke:jokes[1],question:"",punchline:""}} />
        </div>
    )
}

export default JokeApp