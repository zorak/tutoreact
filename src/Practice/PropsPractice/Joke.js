import React from 'react'

function Joke(props){
    return(
        <div>
            <p>
                Pregunta: {props.chistes.joke}
            </p>

            <p style={{display: props.chistes.question ? "block":"none"}}>
                Respuesta: {props.chistes.question}
            </p>
            <p style={{display: props.chistes.question ? "block":"none"}}>
                Punchline: {props.chistes.punchline}
            </p>
            
        </div>
    )
}

export default Joke