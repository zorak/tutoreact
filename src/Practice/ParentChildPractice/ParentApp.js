import React from 'react'
import Navbar from './Navbar'
import MainContent from './MainContent'
import Footer from './Footer'
function ParentApp(){
    return(
        <div>
            <Navbar />
            <MainContent />
            <Footer />
        </div>
    )
}

export default ParentApp