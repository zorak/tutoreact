import React from "react"

class StateAppPractice extends React.Component{
    constructor(){
        super()
        this.state= {
            name:"José",
            age:28
        }
    }
    render(){
        return (
            <div>
                <h1>{this.state.name}</h1>
                <h2>{this.state.age} years old</h2>
                <LogIn />
            </div>
        )
    }
}


class LogIn extends React.Component{
    constructor(){
        super()
        this.state={
            estado:true
        }

    }

    render(){
        let wordDisplay
        if (this.state.estado){
            wordDisplay = "in"
        }else{
            wordDisplay="out"
        }
        return(
            <div>
                <h1>You are currently logged {wordDisplay}  </h1>
            </div>
        )
    }
}


export default StateAppPractice