import React from 'react'
import productsData from './products'
import Product from './Product'

function MappingApp(){
    const productList = productsData.map(product => <Product key ={product.id} item={product} />)
    return(
        <div>
            <h3>Productos disponibles:</h3>
            {productList}
        </div>
    )
}

export default MappingApp