import React from 'react'

function Product(prop){
    return(
        <div>
            <h3>Nombre: {prop.item.name}</h3>
            <p>Precio: {prop.item.price}</p>
            <p style={{display: prop.item.description ? "block":"none"}}>Descripción: {prop.item.description}</p>
        </div>
    )
}

export default Product