const productsData = [
    {
        id:"1", name:"pencil", price:"100",description:"to write with"
    },
    {
        id:"2", name:"eraser", price:"100",description:"to erase"
    },
    {
        id:"3", name:"notebook", price:"100",description:"to write on it"
    },
    {
        id:"4", name:"coffee", price:"100",description:"to drink"
    },
    {
        id:"5", name:"snacks", price:"100",description:"to eat"
    },
    {
        id:"6", name:"rubber duck", price:"100"
    },

]

export default productsData