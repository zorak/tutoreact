import React from 'react';
import logo from './logo.svg';
import './App.css';

//la asignacion de clases es por className, no class porque son JSX, no HTML ********
//const App = () => <div>Componentes</div>
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <div>
          <ul>
            <li>1</li>
            <li>2</li>
            <li>3</li>
          </ul>
        </div>
        
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <div id="root">
        <ul>4</ul>
        <ul>5</ul>
        <ul>6</ul>
    </div>
    </div>
  );
}

export default App;
