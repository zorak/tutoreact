import React from 'react'

class StateChangeApp extends React.Component{
    constructor(){
        super()
        this.state={
            count:0
        }
        this.handleClick=this.handleClick.bind(this)
    }

    handleClick(){
        this.setState(prevState=> {return {count:prevState.count+1}})
    }
    //childcomponent se va a renderizar cada vez que count se actualice
    render(){
        return(
            <div>
                <h1>{this.state.count}</h1>
                <button onClick={this.handleClick}>Change</button>
                <ChildComponent count={this.state.count} />
            </div>
        )
    }
}


export default StateChangeApp