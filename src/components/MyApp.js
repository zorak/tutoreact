import React from 'react'

function MyApp(){
    return (
     <div>
       <h1>Listado por componentes</h1>
       <h2>Encapsulado en un gran div</h2>
       <ul>
         <li>1</li>
         <li>2</li>
         <li>3</li>
       </ul>
     </div>
    )
  }
export default MyApp