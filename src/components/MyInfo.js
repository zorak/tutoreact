import React from 'react'
import Vacaciones from './Vacaciones'
function MyInfo(props){
  const listVacaciones = props.data.map(vacacion => <Vacaciones lugar={vacacion}/>)
    return (
     <div>
      <h1>Información desde archivo separado!</h1>
      <h1>José Zamorano</h1>
      <p>bla bla bla trabajo en IBM :3</p>
      <p>Lugares favoritos para vacaciones:</p>
      {listVacaciones}
    </div>
    )
  }

  export default MyInfo //permite exportar el componente funcional