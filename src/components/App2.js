import React from 'react'
import MyInfo from './MyInfo'
import Vacaciones from './Vacaciones'
import Footer from './Footer' //Llama al componente que genera el footer


//Eventualmente se puede tener un componente que contiene los componentes que deben ser cargados!
/**
 * JSX y javascript:
 * JSX puede utilizar variables definidas dentro de una funcion javascript, pero requiere
 * que las variables vayan dentro de {} para ser tomadas como tal
 */
function App2(){

    const nums = [1,2,3,4,5,6,7,8,9,10]
    const doubled = nums.map(function(num){
        return num*2
    })

    let vacaciones=["Mi casa","La Playa","Mi casa de nuevo"]
    const listVacaciones = vacaciones.map(vacacion => <Vacaciones lugar={vacacion}/>)
    console.log(doubled)
    const firstName = "José";
    const lastName = "Zamorano";
    return(
        <div>
            <nav>
                <h1>Hello world</h1>
                <h2>Nombre : {firstName}</h2>
                <h2>Apellido: {lastName}</h2>
                <ul>
                    <li>Número 1</li>
                    <li>Número 2</li>
                    <li>Número 3</li>
                </ul>
            </nav>
            <main>
                <p>Contenidos</p>
                <MyInfo data = {vacaciones}/>
                <p>listado sacado a partir de constante mapeada en App2</p>
                {listVacaciones}
            </main>
            <Footer />
        </div>
    )
}

export default App2