import React from 'react'

class AppApi extends React.Component{
    constructor(){
        super()
        this.state={
            loading:false,
            swChar:{}
        }
    }

    componentDidMount(){
        this.setState({loading:true})
        fetch("https://swapi.co/api/people/1")
            .then(response => response.json())
            .then(data => {
                this.setState({loading:false, swChar:data})
            })
    }
    


    render(){
        const charName = this.state.loading ?  "Loading data..." : this.state.swChar.name
        
        return(
            <div>
                <h1>Text</h1>
                <h3>{charName}</h3>
                
            </div>
        )
    }
}

export default AppApi