import React from 'react' //react es quien carga JSX. Se debe importar en cada archivo con JSX
import ReactDOM from 'react-dom'
import './index.css'
//import App from './App'
//import MyInfo from './components/MyInfo' 
//import App2 from './components/App2'
//import ParentApp from './Practice/ParentChildPractice/ParentApp'
//import JokeApp from './Practice/PropsPractice/JokeApp'
//import MappingApp from './Practice/MappingPractice/MappingApp'
//import ClassBasedApp from './ClassBasedComponents/ClassBasedApp'
//import ClassBasedComponentPractice from './Practice/ClassBasedComponentsPractice/ClassBasedComponentPractice'
//import StateApp from './State/StateApp'
//import StateAppPractice from './Practice/StatePractice/StateAppPractice'
//import HandleApp from './HandleEvents/HandleApp'
//import StateChangeApp from './ChangeStates/StateChangeApp'
//import App from './ConditionalRendering/App'
//import App2 from './ConditionalRendering/App2'
//import CondRenderApp from './Practice/ConditionalRenderingPractice/CondRenderAppPractice'
//import AppApi from './FetchDataApi/AppApi'
import AppForm from './Forms/AppForms'
import * as serviceWorker from './serviceWorker';
//react asume que estas importando archivos .js, por eso no se especifica el tipo de archivo
//2 parametros, que quiero renderizar y donde lo quiero renderizar
//JSX ***** no se pueden cargar 2 elementos JSX dentro de un parametro
ReactDOM.render(<AppForm />, document.getElementById("root"));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


//Componentes funcionales**
/**
 * Componentes se crean mediante la creacion de funciones. 
 * Las funciones devuelven los componentes JSX que se renderizan. 
 * Para renderizar se crea una instancia que se considera como el primer parametro de ReactDOM.render
 * <MyApp />
 * También deben ir encerrados en un solo objeto JSX
 */