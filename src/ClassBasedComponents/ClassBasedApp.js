import React from 'react'

//function ClassBasedApp(props){
//    return(
//        <div>
//            <h1>Codigo bonito</h1>
//            <h2>{props.whatever}</h2>
//        </div>
//    )
//}

class ClassBasedApp extends React.Component {
    
    metodoPropio(){
        return 'Hola!'
    }
    
    render (){
        const date = new Date();
        //conditional rendering
        const saludo =this.metodoPropio()
        return(
            <div>
                <h1>{saludo} Codigo React</h1>
                <p>{this.props.whatever}</p>
            </div>
        )
    }
}


export default ClassBasedApp