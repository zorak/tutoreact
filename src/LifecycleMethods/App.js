import React from 'react'

class App extends React.Componen{
    constructor(){
        super()
        this.state={}

    }
    
    static getDerivedStateFromProps(props, state){
        //regresa el nuevo (actualizado) state segun props
    }

    //Genera un backup de los valores actuales
    getSnapshotBeforeUpdate(){

    }

    //Se ejecuta la primera vez que el componente se renderiza en pantalla
    //util para recuperar datas desde alguna api externa
    componentDidMount(){

    }

    //Se ejecuta cada vez que el componenete recibe un prop desde un componente padre
    //util para comparar si el prop que se recibe es distinto al prop que esta sinedo utilizado actualmente
    //
    componentWilllReceiveProps(nextProps){
        if(nextProps.whatever !== this.props.whatever){
            //do stuff
        }
    }

    //Optimiza aplicacion para evitar que elementos que no se actualizan regularmente se renderizen
    shouldComponentUpdate(nextProps, nextState){
        //return true to update
        //return false to not update
    }

    //Permite limpiar codigo antes que componente se cierra
    //ej: cerrar event listeners
    componentWillUnpount(){

    }

    render(){
        return (
            <div>
                Test LifecycleMethods
            </div>
        )
    }
}


export default App